package com.mobility.sdk.tests;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.model.fiwaredatamodel.PointOfInterest;
import com.mobility.sdk.OrionCBGetNearestBikeHireDockingStation;
import com.mobility.sdk.OrionCBGetNearestTransportStopsAndLines;

public class OrionCBGetNearestTransportStopsAndLinesTest {
	
	//TEST: Nant
	public static void main(String[] args) {	        
		
	        double lat = -1.59822363;
	        double lon = 47.22357637;
	        int distance = 0;
	        String host="host_nant";
	        String service="service_nant";
	        String servicePath="servicePath_nant";
	        Logger LOGGER = Logger.getLogger(OrionCBGetNearestTransportStopsAndLinesTest.class.getName());

	        LOGGER.log(Level.INFO,"Start!"); // Display the string.
	        
	        OrionCBGetNearestTransportStopsAndLines oNTSAL = new OrionCBGetNearestTransportStopsAndLines();
	        
	        if(oNTSAL!= null) {
		        List<PointOfInterest> nearestEnts = oNTSAL.orionGetNearestTransportStopsAndLines(lat, lon, distance, host, service, servicePath);
		        
		        if(nearestEnts!=null) {
			        for(int i=0; i<nearestEnts.size();i++) {
			        	System.out.println(nearestEnts.get(i).getId());
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No bikes entities could be recovered");
		        }
	        }
       
	        LOGGER.log(Level.INFO,"Finish!");      
	    }	
	
}
