package com.mobility.sdk.tests;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.model.fiwaredatamodel.Vehicle;
import com.mobility.sdk.OrionCBGetNearestBikeHireDockingStation;
import com.mobility.sdk.OrionCBGetNearestBusesList;
import com.mobility.sdk.model.extendedfiwaredatamodel.BikeHireDockingStationExtended;

public class OrionCBGetNearestBusesListTest {
	
	//TEST: Malaga
	public static void main(String[] args) {	        
	        
	        double lat = -4.436191;
	        double lon = 36.705883;
	        int distance = 0;
	        String host="host_malaga";
	        String service="service_malaga";
	        String servicePath="servicePath_malaga";
	        Logger LOGGER = Logger.getLogger(OrionCBGetNearestBusesListTest.class.getName());

	        LOGGER.log(Level.INFO,"Start!"); // Display the string.
	        
	        OrionCBGetNearestBusesList oNBL = new OrionCBGetNearestBusesList();
	        
	        if(oNBL!= null) {
		        List<Vehicle> nearestEnts = oNBL.orionGetNearestBusStations(lat, lon, distance, host, service, servicePath);
		        
		        if(nearestEnts!=null) {
			        for(int i=0; i<nearestEnts.size();i++) {
			        	System.out.println(nearestEnts.get(i).getId());
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No buses entities could be recovered");
		        }
	        }
       
	        LOGGER.log(Level.INFO,"Finish!");      
	    }	
	
}
