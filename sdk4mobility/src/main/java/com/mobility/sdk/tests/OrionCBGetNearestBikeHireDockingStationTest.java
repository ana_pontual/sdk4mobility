package com.mobility.sdk.tests;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.mobility.sdk.OrionCBGetNearestBikeHireDockingStation;
import com.mobility.sdk.model.extendedfiwaredatamodel.BikeHireDockingStationExtended;

public class OrionCBGetNearestBikeHireDockingStationTest {
	
	//TEST: Nant
	public static void main(String[] args) {	        
	        
	        double lat = -1.542847782;
	        double lon = 47.215919436;
	        int distance = 0;
	        String host="host_malaga";
	        String service="service_malaga";
	        String servicePath="servicePath_malaga";
	        Logger LOGGER = Logger.getLogger(OrionCBGetNearestBikeHireDockingStationTest.class.getName());

	        LOGGER.log(Level.INFO,"Start!"); // Display the string.
	        
	        OrionCBGetNearestBikeHireDockingStation oNBHS = new OrionCBGetNearestBikeHireDockingStation();
	        
	        if(oNBHS!= null) {
		        List<BikeHireDockingStationExtended> nearestEnts = oNBHS.orionGetNearestBikeStations(lat, lon, distance, host, service, servicePath);
		        
		        if(nearestEnts!=null) {
			        for(int i=0; i<nearestEnts.size();i++) {
			        	System.out.println(nearestEnts.get(i).getId());
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No bikes entities could be recovered");
		        }
	        }
       
	        LOGGER.log(Level.INFO,"Finish!");      
	    }	
	
}
