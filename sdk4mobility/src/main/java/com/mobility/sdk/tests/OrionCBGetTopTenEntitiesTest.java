package com.mobility.sdk.tests;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mobility.sdk.OrionCBGetTopTenEntities;
import com.mobility.sdk.model.extendedfiwaredatamodel.TrafficFlowObservedExtended;


public class OrionCBGetTopTenEntitiesTest {
	
	//TEST
	public static void main(String[] args) {	        
	        String host="host_madrid";
	        String service="service";
	        String servicePath="servicePath";
	        Logger LOGGER = Logger.getLogger(OrionCBGetTopTenEntitiesTest.class.getName());
	        
	        LOGGER.log(Level.INFO,"Start!"); // Display the string.
	        
	        OrionCBGetTopTenEntities oTT = new OrionCBGetTopTenEntities();
	        
	        if(oTT!= null) {
		        List<TrafficFlowObservedExtended> intensityEnts = oTT.orionGetTopTenIntensity(host, service, servicePath);
		        List<TrafficFlowObservedExtended> loadEnts = oTT.orionGetTopTenLoad(host, service, servicePath);
		        List<TrafficFlowObservedExtended> ocupancyEnts = oTT.orionGetTopTenOcupancy(host, service, servicePath);
	      
		        if(intensityEnts!=null) {
			        for(int i=0; i<intensityEnts.size();i++) {	
			        	LOGGER.log(Level.INFO,intensityEnts.get(i).getId());
			        	//System.out.println(intensityEnts.get(i).getId());	        		        	
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No intensity entities could be recovered");
		        }
		        
		        if(loadEnts!=null) {
			        for(int i=0; i<loadEnts.size();i++) {
			        	LOGGER.log(Level.INFO,loadEnts.get(i).getId());
			        	//System.out.println(loadEnts.get(i).getId());
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No load entities could be recovered");
		        }
		        
		        if(ocupancyEnts!=null) {
			        for(int i=0; i<ocupancyEnts.size();i++) {
			        	LOGGER.log(Level.INFO,ocupancyEnts.get(i).getId());
			        	//System.out.println(ocupancyEnts.get(i).getId());
			        }
		        }else {
		        	LOGGER.log(Level.SEVERE,"No occupancy entities could be recovered");
		        }		        
		        
	        }
	        
	        LOGGER.log(Level.INFO,"Finish!"); 
	    }
	//}
	
}
