package com.mobility.sdk.tools;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.mobility.sdk.OrionCBGetTopTenEntities;

public class Utils {
	private static  String pFileName = "config.properties";	
	
	//Access to the properties file
	public static Properties getPropertiesFile() {
		
		//Get properties values
		Properties prop = new Properties();
		InputStream inStream = OrionCBGetTopTenEntities.class.getClassLoader().getResourceAsStream(pFileName);
		if(inStream!= null) {
			try {
				prop.load(inStream);
			} catch (IOException e) {				
				System.out.println("Error opening properties file:"+pFileName);
				e.printStackTrace();
			}
		}
		return prop;
	}
}
