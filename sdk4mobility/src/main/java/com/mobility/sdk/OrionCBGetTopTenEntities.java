package com.mobility.sdk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.cityenabler.sdk.core.tools.Orion;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.tool.SdkContext;
import com.mobility.sdk.model.extendedfiwaredatamodel.TrafficFlowObservedExtended;
import com.mobility.sdk.serde.extendedfiwaredatamodel.TrafficFlowObservedExtendedSerde;
import com.mobility.sdk.tools.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class OrionCBGetTopTenEntities {
	static Properties prop= Utils.getPropertiesFile();
	private final Logger LOGGER = Logger.getLogger(OrionCBGetTopTenEntities.class.getName());
	
	private TrafficFlowObservedExtendedSerde serde = null;
	
	private List<TrafficFlowObservedExtended> orionGetFilteredEntities(Map<String,String> filter, String host, String service, String servicePath) {
		List<TrafficFlowObservedExtended> list = null;
		
		if(filter != null) {			
			
			SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
			SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());
			
			serde= new TrafficFlowObservedExtendedSerde();
			
			if((host!=null&&service!=null&&servicePath!=null)&&
			   (prop.getProperty(host)!=null)&&(prop.getProperty(service)!=null)&&
			   (prop.getProperty(servicePath)!=null)) {
	
				Orion o = Orion.getInstance(prop.getProperty(host));		
				o.setClient(new HttpClientImpl());	
			
				TrafficFlowObservedExtended[] res = o.getEntities(prop.getProperty(service), prop.getProperty(servicePath), filter, TrafficFlowObservedExtended[].class);
				
				list = new ArrayList<TrafficFlowObservedExtended>(Arrays.asList(res));
			}else {
				LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath);
			}
		}else {
			LOGGER.log(Level.SEVERE, "Error: filter="+filter);
		}
		
		return list;
	}
	
	/*Get Top 10 Intensity*/
	/*Error: returns null*/
	public List<TrafficFlowObservedExtended> orionGetTopTenIntensity(String host, String service, String servicePath) {
		Map<String,String> params = new HashMap<String,String>();
		List<TrafficFlowObservedExtended> res = null;

		if((host!=null&&service!=null&&servicePath!=null)&&
		   (prop.getProperty("type_traffic")!=null)&&(prop.getProperty("orderInte")!=null)&&
		   (prop.getProperty("limit")!=null)) {

			params.put("type", prop.getProperty("type_traffic"));
			params.put("orderBy", prop.getProperty("orderInte"));
			params.put("limit", prop.getProperty("limit"));
			
			res=orionGetFilteredEntities(params,host,service,servicePath);
		}else {
			LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath+"type_traffic="+prop.getProperty("type_traffic")+"orderInte="+prop.getProperty("orderInte")+"limit="+prop.getProperty("limit"));
		}
		
		return res;		
		
	}
	
	/*Get Top 10 Load*/
	/*Error: returns null*/
	public List<TrafficFlowObservedExtended> orionGetTopTenLoad(String host, String service, String servicePath) {
		Map<String,String> params = new HashMap<String,String>();
		List<TrafficFlowObservedExtended> res = null;

		if((host!=null&&service!=null&&servicePath!=null)&&
		   (prop.getProperty("type_traffic")!=null)&&(prop.getProperty("orderLoad")!=null)&&
		   (prop.getProperty("limit")!=null)) {

			params.put("type", prop.getProperty("type_traffic"));
			params.put("orderBy", prop.getProperty("orderLoad"));
			params.put("limit", prop.getProperty("limit"));
			
			res=orionGetFilteredEntities(params, host, service, servicePath);
		}else {
			LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath+"type_traffic="+prop.getProperty("type_traffic")+"orderLoad="+prop.getProperty("orderLoad")+"limit="+prop.getProperty("limit"));
		}
		
		return res;
	}
	
	/*Get Top 10 Ocupancy*/
	/*Error: returns null*/
	public List<TrafficFlowObservedExtended> orionGetTopTenOcupancy(String host, String service, String servicePath) {
		Map<String,String> params = new HashMap<String,String>();
		List<TrafficFlowObservedExtended> res = null;
		
		if((host!=null&&service!=null&&servicePath!=null)&&
		   (prop.getProperty("type_traffic")!=null)&&(prop.getProperty("orderOcu")!=null)&&
		   (prop.getProperty("limit")!=null)) {

			params.put("type", prop.getProperty("type_traffic"));
			params.put("orderBy", prop.getProperty("orderOcu"));
			params.put("limit", prop.getProperty("limit"));
			
			res=orionGetFilteredEntities(params, host, service, servicePath);
		}else {
			LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath+"type_traffic="+prop.getProperty("type_traffic")+"orderOcu="+prop.getProperty("orderOcu")+"limit="+prop.getProperty("limit"));
		}
		
		return res;
	}
	

}
