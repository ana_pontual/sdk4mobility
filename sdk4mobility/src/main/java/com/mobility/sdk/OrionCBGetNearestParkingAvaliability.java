package com.mobility.sdk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.cityenabler.sdk.core.tools.Orion;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.fiwaredatamodel.OffStreetParking;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.tool.SdkContext;
import com.cityenabler.sdk.serde.fiwaredatamodel.OffstreetParkingSerde;
import com.mobility.sdk.tools.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;


public class OrionCBGetNearestParkingAvaliability {
	
	static Properties prop= Utils.getPropertiesFile();
	private final Logger LOGGER = Logger.getLogger(OrionCBGetNearestParkingAvaliability.class.getName());
	
	private OffstreetParkingSerde serde = null;
	
	private List<OffStreetParking> orionGetEntities(Map<String,String> filter, String host, String service, String servicePath) {
		List<OffStreetParking> list = null; 
		
		if(filter != null) {
			SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
			SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());
			
			serde= new OffstreetParkingSerde();
			
			if((host!=null&&service!=null&&servicePath!=null)&&
			   (prop.getProperty(host)!=null)&&(prop.getProperty(service)!=null)&&
			   (prop.getProperty(servicePath)!=null)) {
	
				Orion o = Orion.getInstance(prop.getProperty(host));		
				o.setClient(new HttpClientImpl());	
			
				OffStreetParking[] r = o.getEntities(prop.getProperty(service), prop.getProperty(servicePath), filter, OffStreetParking[].class);
			    
				list = new ArrayList<OffStreetParking>(Arrays.asList(r));
			}else {
				LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath);
			}
		}else {
			LOGGER.log(Level.SEVERE, "Error: filter="+filter);
		}
	
		return list;
	}
	
	/*Get list of parking availability taking into account the user location*/
	public List<OffStreetParking> orionGetNearestParkingAvaliability(Double lat, Double lon, int distance, String host, String service, String servicePath) {
		List<OffStreetParking> res = null;
		
		if((lat!=null)&&(lon!=null)&&
		   (host!=null&&service!=null&&servicePath!=null)) {
			
			String coords = lat+","+lon;			
			String type_transport = prop.getProperty("type_parking");
			String georel_Veh = prop.getProperty("georel_Veh");
			String geometry = prop.getProperty("geometry");
			String order_distance = prop.getProperty("order_distance");
			String limit_pag = prop.getProperty("limit_pag");			
			
			Map<String,String> params = new HashMap<String,String>();		
			if((type_transport!=null)&&(georel_Veh!=null)&&
			   (geometry!=null)&&(order_distance!=null)) {
			     
				String georelPlusDis = georel_Veh;
				georelPlusDis = georelPlusDis+String.valueOf(distance);
				
				params.put("type", type_transport);
				params.put("georel", georelPlusDis);
				params.put("geometry", geometry);
				params.put("coords", coords); 
				params.put("orderBy", order_distance);
				params.put("limit", limit_pag);
				
				//First call				
				List<OffStreetParking> aux = orionGetEntities(params,host,service,servicePath);
				res = aux;
				
				System.out.println("First time returns:"+aux.size());
				
				int off;
				for(int i=1; aux.size() == Integer.parseInt(limit_pag);i++) {
					
					off = Integer.parseInt(limit_pag)*i;
				
					params.put("offset", String.valueOf(off));
					
					aux = orionGetEntities(params,host,service,servicePath);
					System.out.println("In for Aux returns:"+aux.size());
					res.addAll(aux);
					System.out.println("Now res has:"+res.size());
				}
				
			}else {
				LOGGER.log(Level.SEVERE, "Error: type="+type_transport+"georel="+georel_Veh+"geometry="+geometry+"order_distance="+order_distance);
			}
		}else {
			LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath+"latitud="+lat+"longitud="+lon+"distance="+distance);
		}
		return res;
	}

}
