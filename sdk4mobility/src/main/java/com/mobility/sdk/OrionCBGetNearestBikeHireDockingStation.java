package com.mobility.sdk;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.cityenabler.sdk.core.tools.Orion;
import com.cityenabler.sdk.httpclient.impl.HttpClientImpl;
import com.cityenabler.sdk.model.databinding.DefaultDataBinder;
import com.cityenabler.sdk.model.type.builder.fiware.NGSITypeBuilder;
import com.cityenabler.sdk.tool.SdkContext;
import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;
import com.cityenabler.sdk.model.fiwaredatamodel.OffStreetParking;
import com.cityenabler.sdk.serde.fiwaredatamodel.BikeHireDockingStationSerde;
import com.mobility.sdk.model.extendedfiwaredatamodel.BikeHireDockingStationExtended;
import com.mobility.sdk.serde.extendedfiwaredatamodel.BikeHireDockingStationExtendedSerde;
import com.mobility.sdk.tools.Utils;

import java.util.logging.Level;
import java.util.logging.Logger;


public class OrionCBGetNearestBikeHireDockingStation {
	
	static Properties prop= Utils.getPropertiesFile();
	private final Logger LOGGER = Logger.getLogger(OrionCBGetNearestBikeHireDockingStation.class.getName());
	
	private BikeHireDockingStationExtendedSerde serde = null;
	
	private List<BikeHireDockingStationExtended> orionGetEntities(Map<String,String> filter, String host, String service, String servicePath) {
		List<BikeHireDockingStationExtended> list = null; 
		
		if(filter != null) {
			SdkContext.getIstance().setDataBinder(new DefaultDataBinder());
			SdkContext.getIstance().setTypeBuilder(new NGSITypeBuilder());
			
			serde= new BikeHireDockingStationExtendedSerde();
			
			if((host!=null&&service!=null&&servicePath!=null)&&
			   (prop.getProperty(host)!=null)&&(prop.getProperty(service)!=null)&&
			   (prop.getProperty(servicePath)!=null)) {
	
				Orion o = Orion.getInstance(prop.getProperty(host));		
				o.setClient(new HttpClientImpl());	
			
				BikeHireDockingStationExtended[] r = o.getEntities(prop.getProperty(service), prop.getProperty(servicePath), filter, BikeHireDockingStationExtended[].class);
			    
				list = new ArrayList<BikeHireDockingStationExtended>(Arrays.asList(r));
			}else {
				LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath);
			}
		}else {
			LOGGER.log(Level.SEVERE, "Error: filter="+filter);
		}
	
		return list;
	}
	
	/*Get 5 nearest Bike station, giving lat and long*/
	public List<BikeHireDockingStationExtended> orionGetNearestBikeStations(Double lat, Double lon, Integer distance, String host, String service, String servicePath) {
		List<BikeHireDockingStationExtended> res = null;
		
		if((lat!=null)&&(lon!=null)&&
		   (host!=null&&service!=null&&servicePath!=null)) {
			
			String coords = lat+","+lon;
			String type_bike = prop.getProperty("type_bike");
			String georel = prop.getProperty("georel");
			String geometry = prop.getProperty("geometry");
			String order_distance = prop.getProperty("order_distance");
			String limit_pag = prop.getProperty("limit_pag");	
			
			Map<String,String> params = new HashMap<String,String>();		
			if((type_bike!=null)&&(georel!=null)&&
			   (geometry!=null)&&(order_distance!=null)) {
				
				String georelPlusDis = georel;
				georelPlusDis = georelPlusDis+distance.toString();
				
				params.put("type", type_bike);
				params.put("georel", georelPlusDis);
				params.put("geometry", geometry);
				params.put("coords", coords); 
				params.put("orderBy", order_distance);
				params.put("limit", limit_pag);
			
				//First call
				List<BikeHireDockingStationExtended> aux = orionGetEntities(params,host,service,servicePath);
				res = aux;
				
				System.out.println("First time returns:"+aux.size());
				
				int off;
				for(int i=1; aux.size() == Integer.parseInt(limit_pag);i++) {
					
					off = Integer.parseInt(limit_pag)*i;
				
					params.put("offset", String.valueOf(off));
					
					aux = orionGetEntities(params,host,service,servicePath);
					System.out.println("In for Aux returns:"+aux.size());
					res.addAll(aux);
					System.out.println("Now res has:"+res.size());
				}
				
			}else {
				LOGGER.log(Level.SEVERE, "Error: type="+prop.getProperty("type_bike")+"georel="+prop.getProperty("georel")+"geometry="+prop.getProperty("geometry")+"order_distance="+prop.getProperty("order_distance")+"limit_bikes="+prop.getProperty("limit_bikes"));
			}
		}else {
			LOGGER.log(Level.SEVERE, "Error: host="+host+"service="+service+"servicePath="+servicePath+"latitud="+lat+"longitud="+lon);
		}
		return res;
		
	}

}
