package com.mobility.sdk.model.extendedfiwaredatamodel;

import java.util.HashMap;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.fiwaredatamodel.BikeHireDockingStation;

public class BikeHireDockingStationExtended extends BikeHireDockingStation {
	/**
	 * Civic address of this traffic flow observation.
	 * @NormativeReferences https://schema.org/address
	 * @Optional
	 */
	private Optional<NGSIAttribute<HashMap>> address;
	
	public Optional<NGSIAttribute<HashMap>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<HashMap> address) {
		this.address = Optional.of(address);
	}
}
