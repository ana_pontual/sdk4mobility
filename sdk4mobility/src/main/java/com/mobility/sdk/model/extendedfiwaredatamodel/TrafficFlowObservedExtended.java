package com.mobility.sdk.model.extendedfiwaredatamodel;

import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import com.cityenabler.sdk.model.NGSIAttribute;
import com.cityenabler.sdk.model.fiwaredatamodel.TrafficFlowObserved;

public class TrafficFlowObservedExtended extends TrafficFlowObserved {	
	/**
	 * Civic address of this traffic flow observation.
	 * @NormativeReferences https://schema.org/address
	 * @Optional
	 */
	private Optional<NGSIAttribute<HashMap>> address;
	/**
	 * Specific field for Madrid.
	 * @AttributeType String
	 * @Optional
	 */
	private Optional<NGSIAttribute<String>> code;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> intensidadSat;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> intensityAVG;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> load;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> loadAVG;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> nivelServicio;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> occupancyAVG;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Double
	 * @Optional
	 */
	private Optional<NGSIAttribute<Double>> ocupancyAVG;
	/**
	 * Specific field for Madrid.
	 * @AttributeType Boolean
	 * @Optional
	 */
	private Optional<NGSIAttribute<Boolean>> isHoliday;
	/**
	 * The date and time of this observation in ISO8601 UTC format. It can be represented by an specific time instant or by an ISO8601 interval. As a workaround for the lack of support of Orion Context Broker for datetime intervals, it can be used two separate attributes: dateObservedFrom, dateObservedTo.
	 * @AttributeType DateTime
	 * @Mandatory
	 */
	private NGSIAttribute<String> dateObserved;
	
	public Optional<NGSIAttribute<HashMap>> getAddress() {
		return address;
	}
	public void setAddress(NGSIAttribute<HashMap> address) {
		this.address = Optional.of(address);
	}
	public Optional<NGSIAttribute<String>> getCode() {
		return code;
	}
	public void setCode(NGSIAttribute<String> code) {
		this.code = Optional.of(code);
	}
	public NGSIAttribute<String> getDateObserved() {
		return dateObserved;
	}
	public void setDateObserved(NGSIAttribute<String> dateObserved) {
		this.dateObserved = dateObserved;
	}
	public Optional<NGSIAttribute<Double>> getIntensidadSat() {
		return intensidadSat;
	}
	public void setIntensidadSat(NGSIAttribute<Double> intensidadSat) {
		this.intensidadSat = Optional.of(intensidadSat);
	}
	public Optional<NGSIAttribute<Double>> getIntensityAVG() {
		return intensityAVG;
	}
	public void setIntensityAVG(NGSIAttribute<Double> intensityAVG) {
		this.intensityAVG = Optional.of(intensityAVG);
	}
	public Optional<NGSIAttribute<Boolean>> getIsHoliday() {
		return isHoliday;
	}
	public void setIsHoliday(NGSIAttribute<Boolean> isHoliday) {
		this.isHoliday = Optional.of(isHoliday);
	}
	public Optional<NGSIAttribute<Double>> getLoad() {
		return load;
	}
	public void setLoad(NGSIAttribute<Double> load) {
		this.load = Optional.of(load);
	}
	public Optional<NGSIAttribute<Double>> getLoadAVG() {
		return loadAVG;
	}
	public void setLoadAVG(NGSIAttribute<Double> loadAVG) {
		this.loadAVG = Optional.of(loadAVG);
	}
	public Optional<NGSIAttribute<Double>> getNivelServicio() {
		return nivelServicio;
	}
	public void setNivelServicio(NGSIAttribute<Double> nivelServicio) {
		this.nivelServicio = Optional.of(nivelServicio);
	}
	public Optional<NGSIAttribute<Double>> getOccupancyAVG() {
		return occupancyAVG;
	}
	public void setOccupancyAVG(NGSIAttribute<Double> occupancyAVG) {
		this.occupancyAVG = Optional.of(occupancyAVG);
	}
	public Optional<NGSIAttribute<Double>> getOcupancyAVG() {
		return ocupancyAVG;
	}
	public void setOcupancyAVG(NGSIAttribute<Double> ocupancyAVG) {
		this.ocupancyAVG = Optional.of(ocupancyAVG);
	}
}
